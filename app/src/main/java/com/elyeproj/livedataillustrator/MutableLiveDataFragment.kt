package com.elyeproj.livedataillustrator

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_mutable_live_data.*

// Fragment qui affiche la donnée automatiquement mise à jour
class MutableLiveDataFragment : Fragment() {

    // observateur associé à la donnée
    private val changeObserver = Observer<String> { value ->
        value?.let {
            txt_fragment.text = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mutable_live_data, container, false)
    }

    // on récupère l'activité (MutableLiveDataActivity) qui contient la donnée liveDataA et on lui
    // associe un observateur
    // Si le fragment est visible, alors il reçoit les mise à jour de la donnée, sinon il ne se passe rien
    // et surtout ça ne crashe pas l'application
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity as MutableLiveDataActivity).liveDataA.observe(this, changeObserver)
    }
}
