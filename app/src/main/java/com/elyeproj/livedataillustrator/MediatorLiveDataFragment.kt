package com.elyeproj.livedataillustrator

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_mutable_live_data.*

// Fragment qui affiche la donnée automatiquement mise à jour
class MediatorLiveDataFragment : Fragment() {

    // observateur associé à la donnée
    private val changeObserver = Observer<String> { value ->
        value?.let {
            txt_fragment.text = it
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_mediator_live_data, container, false)
    }

    // on récupère l'activité (MediatorLiveDataActivity) qui contient les données liveDataA et liveDataB.
    // on les déclare ensuite comme source potentielle de données (addSource), tout en leur faisant une
    // transformation (ici en ajoutant simplement "A:" ou "B:" devant leur valeur, puis on leur associe un observateur.
    // Si le fragment est visible, alors il reçoit les mise à jour de la donnée qui a été la dernière à avoir été mise
    // à jour quand le fragment est visible, sinon il ne se passe rien, et surtout ça ne crashe pas l'application.
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        val mediatorLiveData = MediatorLiveData<String>()
        mediatorLiveData.addSource((activity as MediatorLiveDataActivity).liveDataA) { mediatorLiveData.value = "A:$it" }
        mediatorLiveData.addSource((activity as MediatorLiveDataActivity).liveDataB) { mediatorLiveData.value = "B:$it" }
        mediatorLiveData.observe(this, changeObserver)
    }
}
