package com.elyeproj.livedataillustrator

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Exemple 1 : les données sont affichées en live dans l'activité (et dans
        // le fragment si celui-ci est visible.
        btn_mutable_livedata.setOnClickListener{
            startActivity(Intent(this, MutableLiveDataActivity::class.java))
        }

        // Exemple 2 : les données sont affichées en live dans l'activité. Elles sont
        // également transformées pour être affichées dans le fragment si celui-ci est visible.
        btn_transformation_map.setOnClickListener{
            startActivity(Intent(this, TransformationMapActivity::class.java))
        }

        // Exemple 3 : les données proviennent de deux sources et sont affichées en live dans l'activité
        // (et dans le fragment si celui-ci est visible).
        // Seules les dernières données modifiées sont affichées (la source est donc A ou B)
        btn_mediator_livedata.setOnClickListener{
            startActivity(Intent(this, MediatorLiveDataActivity::class.java))
        }

        // Exemple 4 : les données proviennent de deux sources et l'utilisateur choisit la
        // source active. Les données sont affichées en live dans l'activité
        // (et dans le fragment si celui-ci est visible).
        // Seules les dernières données modifiées sont affichées (la source est donc A ou B)
        btn_transformation_switchmap.setOnClickListener{
            startActivity(Intent(this, TransformationSwitchMapActivity::class.java))
        }
    }
}
